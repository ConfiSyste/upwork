﻿namespace Asnsf.AdmPersonalizadora.Lib.Auxiliar
{
    public enum EnPermissao
    {
        [Description("Administrador")]
        Administrador = 0,
        [Description("Recursos Humanos")]
        RecursosHumanos = 1,
        [Description("Atendente")]
        Atendente = 2,
        [Description("Operário")]
        Operario = 3
    }

    public enum EnTpAcaoLogin
    {
        [Description("Login")]
        Login = 0,
        [Description("Tentativa Login")]
        TentativaLogin = 1,
        [Description("Logout")]
        Logout = 2
    }

    public enum EnSexo
    {
        [Description("Masculino")]
        Masculino = 0,
        [Description("Feminino")]
        Feminino = 1
    }

    public enum EnCargo
    {
        [Description("Diretor")]
        Diretor = 0,
        [Description("Analista de Recursos Humanos")]
        RecursosHumanos = 1,
        [Description("Vendedor")]
        Vendedor = 2,
        [Description("Atendente")]
        Atendente = 3,
        [Description("Operário")]
        Operario = 4
    }

    public enum EnStatus
    {
        [Description("Ativo")]
        Ativo = 0,
        [Description("Inativo")]
        Inativo = 1
    }

    public enum EnTpPessoa
    {
        [Description("Física")]
        Fisica = 0,
        [Description("Jurídica")]
        Juridica = 1
    }
}