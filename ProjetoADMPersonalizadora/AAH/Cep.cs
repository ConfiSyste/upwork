﻿using Asnsf.AdmPersonalizadora.Lib.Auxiliar;

namespace Asnsf.AdmPersonalizadora.Lib.Entidade
{
    public class Cep
    {
        public string Logradouro { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }

        public bool CepPreenchido
            => !Logradouro.ParseIsNullOrEmpty()
            && !Bairro.ParseIsNullOrEmpty()
            && !Cidade.ParseIsNullOrEmpty()
            && !Estado.ParseIsNullOrEmpty();
    }
}