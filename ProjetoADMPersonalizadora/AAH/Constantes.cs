﻿using System;
using System.Drawing;
using ProjetoADMPersonalizadora.AAH;

namespace Asnsf.AdmPersonalizadora.Lib.Auxiliar
{
    public static class Constantes
    {
        public static int IdEmpresa => 1;
        public static string ProjectName => AppDomain.CurrentDomain.FriendlyName;
        private static string DsFirstItemDropValue => "-1";
        private static string DsFirstItemDropText => "Selecione...";
        public static VText DsFirstItemDrop => new VText
        {
            Value = DsFirstItemDropValue,
            Text = DsFirstItemDropText
        };

        public static string ExceptionMsg = "MY_EXCEPTION";
        public static string MsgRegsSalvo => "Registros salvos com sucesso!";
        public static string MsgRegsNulo => "Há campos sem preencher!";
        public static string MsgErro => "Ocorreu o seguinte erro no sistema: \n{0}";
        public static string MsgListaNula => "Não há registros à serem exibidos!";
        public static string MsgExcluirRegs => "Deseja realmente excluir este registro?";
        public static string MsgRegsExcluido => "Registros excluídos com sucesso!";

        public static Color BackColorNullField => Color.LightCoral;
        public static Color BackColorNotNullField => Color.White;

        

        #region Mensagens por formulários
        public static string MsgPorFormBancoDuplicado => "Este Nº de Banco já existe em nosso Banco de Dados!";
        public static string MsgPorFormCpfDuplicado => "Este CPF já existe em nosso Banco de Dados!";
        public static string MsgPorFormCpfCnpjDuplicado => "Este CPF/CNPJ já existe em nosso Banco de Dados!";
        public static string MsgPorFormUsuarioDuplicado => "Este Usuário já existe em nosso Banco de Dados!";
        public static string MsgPorFormExcluirBancoComFuncionarioExistente => "Não é possível excluir este banco, pois há funcionários associados a ele!";
        public static string MsgPorFormExcluirCargoComFuncionarioExistente => "Não é possível excluir este cargo, pois há funcionários associados a ele!";
        public static string MsgPorFormInativarCargoComFuncionarioExistente => "Não é possível inativar este cargo, pois há funcionários associados a ele!";
        #endregion
    }
}
