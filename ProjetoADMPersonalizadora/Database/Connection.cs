﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoADMPersonalizadora.Database
{
    class Connection
    {
        public MySqlConnection Create()
        {
            string connectionString = "server=localhost;database=UPWORKDB;uid=root;password=12345678;sslmode=none";

            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            return connection;
        }
    }
}
