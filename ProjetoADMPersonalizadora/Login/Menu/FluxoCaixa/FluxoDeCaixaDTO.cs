﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoADMPersonalizadora.Login.Menu.Orcamento
{
    public class FluxoDeCaixaDTO
    {
        public int ID_FLUXO_CAIXA { get; set; }
        public int ID_PEDIDO_COMPRA { get; set; }
        public int ID_ORCAMENTO_VENDA { get; set; }
        public int ID_DESPESA { get; set; }
        public DateTime DT_FLUXO_CAIXA { get; set; }
    }

}
