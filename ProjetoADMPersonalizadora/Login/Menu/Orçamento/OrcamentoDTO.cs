﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoADMPersonalizadora.Login.Menu.Orçamento
{
    public class OrcamentoDTO
    {
        public int ID_ORCAMENTO { get; set; }
        public int ID_CLIENTE { get; set; }
        public int ID_FUNCIONARIO { get; set; }
        public DateTime DT_ORCAMENTO { get; set; }
        public DateTime DT_FINAL_ORCAMENTO { get; set; }
        public int ID_MESA { get; set; }
    }

}
