﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoADMPersonalizadora.Login.Menu.Orcamento
{
    public class DespesasDTO
    {
        public int ID_DESPESAS { get; set; }
        public string CL_DESPESA { get; set; }
        public decimal VL_DESPESA { get; set; }
        public DateTime DT_DESPESA { get; set; }
    }

}
