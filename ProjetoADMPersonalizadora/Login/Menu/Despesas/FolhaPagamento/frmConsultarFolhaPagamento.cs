﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoADMPersonalizadora.Login.Menu.Despesas.Folha_de_Pagamento
{
    public partial class frmConsultarFolhaPagamento : Form
    {
        public frmConsultarFolhaPagamento()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new FrmMenu().Show();
            Close();
        }
    }
}
