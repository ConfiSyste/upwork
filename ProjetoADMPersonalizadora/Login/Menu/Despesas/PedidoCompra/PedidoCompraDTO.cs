﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoADMPersonalizadora.Login.Menu.PedidoCompra
{
    public class PedidoCompraDTO
    {
        public int ID_PEDIDO_COMPRA { get; set; }
        public int ID_FORNECEDOR { get; set; }
        public string QT_PRODUTO { get; set; }
        public string VL_PEDIDO_COMPRA { get; set; }
        public DateTime DT_PEDIDO_COMPRA { get; set; }
    }
   
}
