﻿namespace ProjetoADMPersonalizadora.Login.Menu.PedidoCompra
{
    partial class FrmPedidoCompra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFornecedorCompra = new System.Windows.Forms.Label();
            this.txtQuantidadeCompra = new System.Windows.Forms.TextBox();
            this.lblQuantidadeCompra = new System.Windows.Forms.Label();
            this.txtTotalCompra = new System.Windows.Forms.TextBox();
            this.lblTotalCompra = new System.Windows.Forms.Label();
            this.txtValorCompra = new System.Windows.Forms.TextBox();
            this.lblValorCompra = new System.Windows.Forms.Label();
            this.cbbFornecedor = new System.Windows.Forms.ComboBox();
            this.lblProdutoCompra = new System.Windows.Forms.Label();
            this.cbbProduto = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblFornecedorCompra
            // 
            this.lblFornecedorCompra.AutoSize = true;
            this.lblFornecedorCompra.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFornecedorCompra.Location = new System.Drawing.Point(64, 61);
            this.lblFornecedorCompra.Name = "lblFornecedorCompra";
            this.lblFornecedorCompra.Size = new System.Drawing.Size(103, 21);
            this.lblFornecedorCompra.TabIndex = 0;
            this.lblFornecedorCompra.Text = "Fornecedor:";
            // 
            // txtQuantidadeCompra
            // 
            this.txtQuantidadeCompra.Location = new System.Drawing.Point(173, 114);
            this.txtQuantidadeCompra.Name = "txtQuantidadeCompra";
            this.txtQuantidadeCompra.Size = new System.Drawing.Size(199, 20);
            this.txtQuantidadeCompra.TabIndex = 3;
            // 
            // lblQuantidadeCompra
            // 
            this.lblQuantidadeCompra.AutoSize = true;
            this.lblQuantidadeCompra.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidadeCompra.Location = new System.Drawing.Point(55, 114);
            this.lblQuantidadeCompra.Name = "lblQuantidadeCompra";
            this.lblQuantidadeCompra.Size = new System.Drawing.Size(112, 21);
            this.lblQuantidadeCompra.TabIndex = 2;
            this.lblQuantidadeCompra.Text = "Quantidade:";
            // 
            // txtTotalCompra
            // 
            this.txtTotalCompra.Enabled = false;
            this.txtTotalCompra.Location = new System.Drawing.Point(173, 166);
            this.txtTotalCompra.Name = "txtTotalCompra";
            this.txtTotalCompra.Size = new System.Drawing.Size(199, 20);
            this.txtTotalCompra.TabIndex = 7;
            // 
            // lblTotalCompra
            // 
            this.lblTotalCompra.AutoSize = true;
            this.lblTotalCompra.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalCompra.Location = new System.Drawing.Point(114, 166);
            this.lblTotalCompra.Name = "lblTotalCompra";
            this.lblTotalCompra.Size = new System.Drawing.Size(53, 21);
            this.lblTotalCompra.TabIndex = 6;
            this.lblTotalCompra.Text = "Total:";
            // 
            // txtValorCompra
            // 
            this.txtValorCompra.Enabled = false;
            this.txtValorCompra.Location = new System.Drawing.Point(173, 140);
            this.txtValorCompra.Name = "txtValorCompra";
            this.txtValorCompra.Size = new System.Drawing.Size(199, 20);
            this.txtValorCompra.TabIndex = 5;
            // 
            // lblValorCompra
            // 
            this.lblValorCompra.AutoSize = true;
            this.lblValorCompra.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorCompra.Location = new System.Drawing.Point(113, 140);
            this.lblValorCompra.Name = "lblValorCompra";
            this.lblValorCompra.Size = new System.Drawing.Size(54, 21);
            this.lblValorCompra.TabIndex = 4;
            this.lblValorCompra.Text = "Valor:";
            // 
            // cbbFornecedor
            // 
            this.cbbFornecedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbFornecedor.FormattingEnabled = true;
            this.cbbFornecedor.Location = new System.Drawing.Point(173, 60);
            this.cbbFornecedor.Name = "cbbFornecedor";
            this.cbbFornecedor.Size = new System.Drawing.Size(199, 21);
            this.cbbFornecedor.TabIndex = 9;
            // 
            // lblProdutoCompra
            // 
            this.lblProdutoCompra.AutoSize = true;
            this.lblProdutoCompra.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProdutoCompra.Location = new System.Drawing.Point(91, 87);
            this.lblProdutoCompra.Name = "lblProdutoCompra";
            this.lblProdutoCompra.Size = new System.Drawing.Size(76, 21);
            this.lblProdutoCompra.TabIndex = 11;
            this.lblProdutoCompra.Text = "Produto:";
            // 
            // cbbProduto
            // 
            this.cbbProduto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbProduto.FormattingEnabled = true;
            this.cbbProduto.Location = new System.Drawing.Point(173, 87);
            this.cbbProduto.Name = "cbbProduto";
            this.cbbProduto.Size = new System.Drawing.Size(199, 21);
            this.cbbProduto.TabIndex = 12;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(81, 24);
            this.button1.TabIndex = 73;
            this.button1.Text = "Voltar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(184, 200);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(98, 38);
            this.button2.TabIndex = 74;
            this.button2.Text = "Comprar";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // FrmPedidoCompra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(469, 250);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cbbProduto);
            this.Controls.Add(this.lblProdutoCompra);
            this.Controls.Add(this.cbbFornecedor);
            this.Controls.Add(this.txtTotalCompra);
            this.Controls.Add(this.lblTotalCompra);
            this.Controls.Add(this.txtValorCompra);
            this.Controls.Add(this.lblValorCompra);
            this.Controls.Add(this.txtQuantidadeCompra);
            this.Controls.Add(this.lblQuantidadeCompra);
            this.Controls.Add(this.lblFornecedorCompra);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmPedidoCompra";
            this.Text = "FrmPedidoCompra";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFornecedorCompra;
        private System.Windows.Forms.TextBox txtQuantidadeCompra;
        private System.Windows.Forms.Label lblQuantidadeCompra;
        private System.Windows.Forms.TextBox txtTotalCompra;
        private System.Windows.Forms.Label lblTotalCompra;
        private System.Windows.Forms.TextBox txtValorCompra;
        private System.Windows.Forms.Label lblValorCompra;
        private System.Windows.Forms.ComboBox cbbFornecedor;
        private System.Windows.Forms.Label lblProdutoCompra;
        private System.Windows.Forms.ComboBox cbbProduto;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}