﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoADMPersonalizadora.Login.Menu.Orcamento
{
    public class ClienteDTO
    {
        public int ID_CLIENTE { get; set; }
        public string NM_CLIENTE { get; set; }
        public string DS_CPF { get; set; }
        public string DS_CEP { get; set; }
        public string DS_COMPLEMENTO { get; set; }
        public string N_CLIENTE { get; set; }
        public string DS_TELEFONE { get; set; }
        public string DS_EMAIL { get; set; }
        public DateTime DT_NASCIMENTO { get; set; }
    }

}
