﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoADMPersonalizadora.Login.Menu.Funcionários
{
    public class PermissaoDTO
    {
        public int ID_PERMISSAO { get; set; }
        public string NM_PERMISSAO { get; set; }
    }
}
