﻿using ProjetoADMPersonalizadora.Login.Menu.Orcamentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoADMPersonalizadora.Login.Menu.Orcamento
{
    public class FuncionariosBusiness
    {
        public int CADASTRAR(FuncionariosDTO dto)
        {
            return new FuncionariosDatabase().CADASTRAR(dto);
        }
        public List<FuncionariosDTO> LISTAR()
        { 
            return new FuncionariosDatabase().LISTAR();
        }
        public List<FuncionariosDTO> ConsultarPorCpf(string cpf)
        {
            return new FuncionariosDatabase().ConsultarPorCpf(cpf);
        }
    }
}
