﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoADMPersonalizadora.Login.Menu.Orcamento
{
    public class FuncionariosDTO
    {
        public int ID_FUNCIONARIO { get; set; }
        public int ID_PERMISSAO { get; set; }
        public string NM_FUNCIONARIO { get; set; }
        public string USUARIO_FUNCIONARIO { get; set; }
        public string SENHA_FUNCIONARIO { get; set; }
        public string EMAIL_FUNCIONARIO { get; set; }
        public string CARGO_FUNCIONARIO { get; set; }
        public string CPF_FUNCIONARIO { get; set; }
        public DateTime DT_CADASTRO_FUNCIONARIO { get; set; }
        public DateTime DT_NASCIMENTO { get; set; }
        public string AT_FUNCIONARIO { get; set; }
    }

}
