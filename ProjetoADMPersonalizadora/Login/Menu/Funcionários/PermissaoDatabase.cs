﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoADMPersonalizadora.Login.Menu.Funcionários
{
    public class PermissaoDatabase
    {
        public List<PermissaoDTO> LISTAR()
        {
            string script = @"SELECT * FROM TB_PERMISSAO";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database.Database db = new Database.Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PermissaoDTO> lista = new List<PermissaoDTO>();
            while (reader.Read())
            {
                PermissaoDTO dto = new PermissaoDTO();
                dto.ID_PERMISSAO = reader.GetInt32("ID_PERMISSAO");
                dto.NM_PERMISSAO = reader.GetString("NM_PERMISSAO");
                
                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
