﻿using ProjetoADMPersonalizadora.Login.Menu.Orcamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoADMPersonalizadora.Login.Menu.Funcionários
{
    public partial class frmConsultarFuncionario : Form
    {
        public frmConsultarFuncionario()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new FrmMenu().Show();
            Close();
        }

        private void frmConsultarFuncionario_Load(object sender, EventArgs e)
        {

            FuncionariosBusiness bu = new FuncionariosBusiness();
            List<FuncionariosDTO> lista = bu.LISTAR();

            dgvConsultar.AutoGenerateColumns = false;
            dgvConsultar.DataSource = lista;           
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            string cpf = txtCpf.Text.Trim();


            FuncionariosBusiness bu = new FuncionariosBusiness();
            List<FuncionariosDTO> lista = bu.ConsultarPorCpf(txtCpf.Text);

            dgvConsultar.AutoGenerateColumns = false;
            dgvConsultar.DataSource = lista;


        }
        
    }
}
