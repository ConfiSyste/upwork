﻿using Asnsf.AdmPersonalizadora.Lib.Auxiliar;
using ProjetoADMPersonalizadora.Login.Menu.Orcamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoADMPersonalizadora.Login.Menu.Funcionários
{
    public partial class frmCadastrarFuncionario : Form
    {
        private bool _nNEntered;

        public frmCadastrarFuncionario()
        {
            InitializeComponent();
            CarregarCombos();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new FrmMenu().Show();
            Close();
        }

        private void btnInscrever_Click(object sender, EventArgs e)
        {
            try
            {
                string nome = txtNome.Text;
                string email = txtEmail.Text;
                string usuario = txtUsuario.Text;
                string senha = txtSenha.Text;
                string confirmar = txtConfirmar.Text;
                string cpf = txtCPF.Text;
                string cargo = cbbCargo.SelectedItem.ParseToString();
                int permissao = cbbPermissao.SelectedIndex + 1;
                DateTime nascimento = dtpNascimento.Value;
                string atividade = cbbAtiv.SelectedItem.ParseToString();

                FuncionariosDTO dto = new FuncionariosDTO();
                dto.NM_FUNCIONARIO = nome;
                dto.EMAIL_FUNCIONARIO = email;
                dto.USUARIO_FUNCIONARIO = usuario;
                dto.SENHA_FUNCIONARIO = senha;
                dto.DT_CADASTRO_FUNCIONARIO = DateTime.Now;
                dto.CPF_FUNCIONARIO = cpf;
                dto.CARGO_FUNCIONARIO = cargo;
                dto.ID_PERMISSAO = permissao;
                dto.AT_FUNCIONARIO = atividade;
                dto.DT_NASCIMENTO = nascimento;

                FuncionariosBusiness bu = new FuncionariosBusiness();
                bu.CADASTRAR(dto);

                MessageBox.Show("Salvo", "", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK);

            }
        }
        private void CarregarCombos()
        {
            PermissaoBusiness b = new PermissaoBusiness();
            List<PermissaoDTO> l = b.LISTAR();

            cbbPermissao.ValueMember = nameof(PermissaoDTO.ID_PERMISSAO);
            cbbPermissao.DisplayMember = nameof(PermissaoDTO.NM_PERMISSAO);
            cbbPermissao.DataSource = l;
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e) { Functions.ValidarTextoKeyPress(sender, ref e, 100); }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e) { Functions.ValidarEmailKeyPress(sender, ref e, 100); }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e){ Functions.ValidarTextoNumeroKeyPress(sender, ref e, 50);}

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e) { Functions.ValidarSenhaKeyPress(sender, ref e, 50); }

        private void txtConfirmar_KeyPress(object sender, KeyPressEventArgs e) { Functions.ValidarSenhaKeyPress(sender, ref e, 50); }

        private void txtCPF_KeyPress(object sender, KeyPressEventArgs e) { Functions.JustNumbersKPress(ref _nNEntered, ref e); }

        private void txtCPF_KeyDown(object sender, KeyEventArgs e){ Functions.JustNumbersKDown(ref _nNEntered, ref e); }

        private void txtCPF_KeyUp(object sender, KeyEventArgs e) { Functions.FormatCpf((TextBox)sender); }

        private void txtCPF_Leave(object sender, EventArgs e){Functions.FormatCpf((TextBox)sender);}
    }
}
