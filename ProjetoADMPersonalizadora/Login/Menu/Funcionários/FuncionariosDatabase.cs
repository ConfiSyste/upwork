﻿using MySql.Data.MySqlClient;
using ProjetoADMPersonalizadora.Login.Menu.Orcamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoADMPersonalizadora.Login.Menu.Orcamentos
{
    class FuncionariosDatabase
    {
        public int CADASTRAR (FuncionariosDTO dto)
        {
            string script = @"INSERT INTO TB_FUNCIONARIO 
                                (ID_PERMISSAO, NM_FUNCIONARIO, USUARIO_FUNCIONARIO, SENHA_FUNCIONARIO, EMAIL_FUNCIONARIO, CARGO_FUNCIONARIO, CPF_FUNCIONARIO,DT_NASCIMENTO, DT_CADASTRO_FUNCIONARIO, AT_FUNCIONARIO) 
                      VALUES (@ID_PERMISSAO,@NM_FUNCIONARIO,@USUARIO_FUNCIONARIO,@SENHA_FUNCIONARIO,@EMAIL_FUNCIONARIO,@CARGO_FUNCIONARIO,@CPF_FUNCIONARIO,@DT_NASCIMENTO, now(),@AT_FUNCIONARIO);";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PERMISSAO", dto.ID_PERMISSAO));
            parms.Add(new MySqlParameter("NM_FUNCIONARIO", dto.NM_FUNCIONARIO));
            parms.Add(new MySqlParameter("USUARIO_FUNCIONARIO", dto.USUARIO_FUNCIONARIO));
            parms.Add(new MySqlParameter("SENHA_FUNCIONARIO", dto.SENHA_FUNCIONARIO));
            parms.Add(new MySqlParameter("EMAIL_FUNCIONARIO", dto.EMAIL_FUNCIONARIO));
            parms.Add(new MySqlParameter("CARGO_FUNCIONARIO", dto.CARGO_FUNCIONARIO));
            parms.Add(new MySqlParameter("CPF_FUNCIONARIO", dto.CPF_FUNCIONARIO));
            parms.Add(new MySqlParameter("DT_CADASTRO_FUNCIONARIO", dto.DT_CADASTRO_FUNCIONARIO));
            parms.Add(new MySqlParameter("DT_NASCIMENTO", dto.DT_NASCIMENTO));
            parms.Add(new MySqlParameter("AT_FUNCIONARIO", dto.AT_FUNCIONARIO));

            Database.Database db = new Database.Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<FuncionariosDTO> LISTAR()
        {
            string script = @"SELECT * FROM TB_FUNCIONARIO";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database.Database db = new Database.Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionariosDTO> lista = new List<FuncionariosDTO>();
            while (reader.Read())
            {
                FuncionariosDTO dto = new FuncionariosDTO();
                dto.ID_FUNCIONARIO = reader.GetInt32("ID_FUNCIONARIO");
                dto.ID_PERMISSAO = reader.GetInt32("ID_PERMISSAO");
                dto.NM_FUNCIONARIO = reader.GetString("NM_FUNCIONARIO");
                dto.USUARIO_FUNCIONARIO = reader.GetString("USUARIO_FUNCIONARIO");
                dto.SENHA_FUNCIONARIO = reader.GetString("SENHA_FUNCIONARIO");
                dto.EMAIL_FUNCIONARIO = reader.GetString("EMAIL_FUNCIONARIO");
                dto.CARGO_FUNCIONARIO = reader.GetString("CARGO_FUNCIONARIO");
                dto.CPF_FUNCIONARIO = reader.GetString("CPF_FUNCIONARIO");
                dto.DT_CADASTRO_FUNCIONARIO = reader.GetDateTime("DT_CADASTRO_FUNCIONARIO");
                dto.DT_NASCIMENTO = reader.GetDateTime("DT_NASCIMENTO");
                dto.AT_FUNCIONARIO = reader.GetString("AT_FUNCIONARIO");
        
                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<FuncionariosDTO> ConsultarPorCpf(string cpf)
        {
            string script = @"SELECT * FROM TB_FUNCIONARIO WHERE CPF_FUNCIONARIO like @CPF_FUNCIONARIO";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("CPF_FUNCIONARIO", "%" + cpf + "%"));

            var db = new Database.Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            var lista = new List<FuncionariosDTO>();
            while (reader.Read())
            {
                lista.Add(new FuncionariosDTO
                {
                    ID_FUNCIONARIO = reader.GetInt32("ID_FUNCIONARIO"),
                    ID_PERMISSAO = reader.GetInt32("ID_PERMISSAO"),
                    NM_FUNCIONARIO = reader.GetString("NM_FUNCIONARIO"),
                    USUARIO_FUNCIONARIO = reader.GetString("USUARIO_FUNCIONARIO"),
                    SENHA_FUNCIONARIO = reader.GetString("SENHA_FUNCIONARIO"),
                    EMAIL_FUNCIONARIO = reader.GetString("EMAIL_FUNCIONARIO"),
                    CARGO_FUNCIONARIO = reader.GetString("CARGO_FUNCIONARIO"),
                    CPF_FUNCIONARIO = reader.GetString("CPF_FUNCIONARIO"),
                    DT_CADASTRO_FUNCIONARIO = reader.GetDateTime("DT_CADASTRO_FUNCIONARIO"),
                    DT_NASCIMENTO = reader.GetDateTime("DT_NASCIMENTO"),
                    AT_FUNCIONARIO = reader.GetString("AT_FUNCIONARIO")
                });
            }
            reader.Close();

            return lista;
        }
    }
}
