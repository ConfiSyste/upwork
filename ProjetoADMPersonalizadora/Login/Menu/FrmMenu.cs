﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoADMPersonalizadora.Login.Menu
{
    public partial class FrmMenu : Form
    {
        public FrmMenu()
        {
            InitializeComponent();
            Permissões();
        }

        void Permissões()
        {
            
            if (SessaoUsuario.UsuarioLogado.ID_PERMISSAO == 2)
            { 
                funcionáriosToolStripMenuItem.Visible = false;
                fornecedorToolStripMenuItem.Visible = false;
                fluxoDeCaixaToolStripMenuItem.Visible = false;
                pagamentoToolStripMenuItem.Visible = false;

            }
            if (SessaoUsuario.UsuarioLogado.ID_FUNCIONARIO == 3)
            {
                clienteToolStripMenuItem.Visible = false;
            }
        }

        private void FrmMenu_Load(object sender, EventArgs e)
        {

        }

        private void fluxoDeCaixaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ganhosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void orçamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void novoFuncionárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Funcionários.frmCadastrarFuncionario tela = new Funcionários.frmCadastrarFuncionario();
            tela.Show();
            Close();
        }

        private void alterarFuncionárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Funcionários.frmConsultarFuncionario tela = new Funcionários.frmConsultarFuncionario();
            tela.Show();
            Close();
        }

        private void fornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void novoFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Fornecedor.frmCadastrarFornecedor ir = new Fornecedor.frmCadastrarFornecedor();
            ir.Show();
            Close();
        }

        private void consultarFornecedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Fornecedor.frmConsultarFornecedor ir = new Fornecedor.frmConsultarFornecedor();
            ir.Show();
            Close();
        }

        private void novoClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cliente.frmCadastrarCliente ir = new Cliente.frmCadastrarCliente();
            ir.Show();
            Close();

        }

        private void consultarClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cliente.frmConsultarCliente ir = new Cliente.frmConsultarCliente();
            ir.Show();
            Close();
        }

        private void fluxoDeCaixaToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Orçamento.FrmOrcamento ir = new Orçamento.FrmOrcamento();
            ir.Show();
            Close();
        }

        private void novoPedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PedidoCompra.FrmPedidoCompra ir = new PedidoCompra.FrmPedidoCompra();
            ir.Show();
            Close();
        }

        private void consultarPedidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PedidoCompra.frmConsultarPedidoCompra ir = new PedidoCompra.frmConsultarPedidoCompra();
            ir.Show();
            Close();
        }


        private void mensaisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void adicionarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Despesas.Despesas_Mensais.frmAdiconarDepesaMensal ir = new Despesas.Despesas_Mensais.frmAdiconarDepesaMensal();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Despesas.Despesas_Mensais.frmConsultarDepesasAdicionais ir = new Despesas.Despesas_Mensais.frmConsultarDepesasAdicionais();
            ir.Show();
            Close();
        }

        private void adicionarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Despesas.Adicionais.frmDespesa ir = new Despesas.Adicionais.frmDespesa();
            ir.Show();
            Close();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Despesas.Adicionais.frmConsultarDespesa ir = new Despesas.Adicionais.frmConsultarDespesa();
            ir.Show();
            Close();
        }

        private void pedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pedidoToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void novoPedidoToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void gerarOrçamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Orçamento.FrmOrcamento ir = new Orçamento.FrmOrcamento();
            ir.Show();
            Close();
        }

        private void gerarFolhaDePagamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Despesas.Folha_de_Pagamento.frmFolhaDePagamento ir = new Despesas.Folha_de_Pagamento.frmFolhaDePagamento();
            ir.Show();
            Close();
        }

        private void consultarFolhasDePagamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Despesas.Folha_de_Pagamento.frmConsultarFolhaPagamento ir = new Despesas.Folha_de_Pagamento.frmConsultarFolhaPagamento();
            ir.Show();
            Close();
        }

        private void pbClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pbReturn_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
