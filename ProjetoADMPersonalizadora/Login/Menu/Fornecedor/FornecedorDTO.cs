﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoADMPersonalizadora.Login.Menu.Orcamento
{
    public class FornecedorDTO
    {
        public int ID_FORNECEDOR { get; set; }
        public string RAZAO_SOCIAL { get; set; }
        public string CNPJ_FORNECEDOR { get; set; }
        public string TIPOS_PRODUTOS { get; set; }
        public string CEP_FORNECEDOR { get; set; }
        public string TL_FORNECEDOR { get; set; }
    }

}
