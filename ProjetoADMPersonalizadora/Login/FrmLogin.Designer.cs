﻿namespace ProjetoADMPersonalizadora.NewFolder1
{
    partial class FrmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtSenha = new System.Windows.Forms.MaskedTextBox();
            this.btnEntrar = new System.Windows.Forms.Button();
            this.lblEsqueceuConta = new System.Windows.Forms.LinkLabel();
            this.txtUsuario = new System.Windows.Forms.MaskedTextBox();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.lblSenha = new System.Windows.Forms.Label();
            this.chkLembrarSenha = new System.Windows.Forms.CheckBox();
            this.lblUPWork = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtSenha
            // 
            this.txtSenha.Location = new System.Drawing.Point(158, 121);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Size = new System.Drawing.Size(142, 20);
            this.txtSenha.TabIndex = 1;
            this.txtSenha.Text = "1234";
            this.txtSenha.UseSystemPasswordChar = true;
            this.txtSenha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSenha_KeyPress);
            // 
            // btnEntrar
            // 
            this.btnEntrar.Font = new System.Drawing.Font("Century Gothic", 14F);
            this.btnEntrar.Location = new System.Drawing.Point(306, 95);
            this.btnEntrar.Name = "btnEntrar";
            this.btnEntrar.Size = new System.Drawing.Size(114, 47);
            this.btnEntrar.TabIndex = 2;
            this.btnEntrar.Text = "Entrar";
            this.btnEntrar.UseVisualStyleBackColor = true;
            this.btnEntrar.Click += new System.EventHandler(this.btnEntrar_Click);
            // 
            // lblEsqueceuConta
            // 
            this.lblEsqueceuConta.AutoSize = true;
            this.lblEsqueceuConta.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEsqueceuConta.Location = new System.Drawing.Point(151, 173);
            this.lblEsqueceuConta.Name = "lblEsqueceuConta";
            this.lblEsqueceuConta.Size = new System.Drawing.Size(165, 21);
            this.lblEsqueceuConta.TabIndex = 3;
            this.lblEsqueceuConta.TabStop = true;
            this.lblEsqueceuConta.Text = "Esqueçeu a Conta?";
            this.lblEsqueceuConta.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // txtUsuario
            // 
            this.txtUsuario.Location = new System.Drawing.Point(158, 95);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(142, 20);
            this.txtUsuario.TabIndex = 0;
            this.txtUsuario.Text = "gg";
            this.txtUsuario.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtUsuario_MaskInputRejected);
            this.txtUsuario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUsuario_KeyPress);
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.BackColor = System.Drawing.Color.Transparent;
            this.lblUsuario.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblUsuario.Location = new System.Drawing.Point(82, 95);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(70, 21);
            this.lblUsuario.TabIndex = 5;
            this.lblUsuario.Text = "Usuário:";
            // 
            // lblSenha
            // 
            this.lblSenha.AutoSize = true;
            this.lblSenha.BackColor = System.Drawing.Color.Transparent;
            this.lblSenha.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblSenha.Location = new System.Drawing.Point(89, 121);
            this.lblSenha.Name = "lblSenha";
            this.lblSenha.Size = new System.Drawing.Size(63, 21);
            this.lblSenha.TabIndex = 6;
            this.lblSenha.Text = "Senha:";
            // 
            // chkLembrarSenha
            // 
            this.chkLembrarSenha.AutoSize = true;
            this.chkLembrarSenha.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.chkLembrarSenha.Location = new System.Drawing.Point(158, 147);
            this.chkLembrarSenha.Name = "chkLembrarSenha";
            this.chkLembrarSenha.Size = new System.Drawing.Size(130, 23);
            this.chkLembrarSenha.TabIndex = 7;
            this.chkLembrarSenha.Text = "Lembrar senha";
            this.chkLembrarSenha.UseVisualStyleBackColor = true;
            // 
            // lblUPWork
            // 
            this.lblUPWork.AutoSize = true;
            this.lblUPWork.Font = new System.Drawing.Font("Century Gothic", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUPWork.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.lblUPWork.Location = new System.Drawing.Point(150, 24);
            this.lblUPWork.Name = "lblUPWork";
            this.lblUPWork.Size = new System.Drawing.Size(183, 47);
            this.lblUPWork.TabIndex = 8;
            this.lblUPWork.Text = "UP-Work";
            // 
            // FrmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(492, 217);
            this.Controls.Add(this.lblUPWork);
            this.Controls.Add(this.chkLembrarSenha);
            this.Controls.Add(this.lblSenha);
            this.Controls.Add(this.lblUsuario);
            this.Controls.Add(this.txtUsuario);
            this.Controls.Add(this.lblEsqueceuConta);
            this.Controls.Add(this.btnEntrar);
            this.Controls.Add(this.txtSenha);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.FrmLogin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox txtSenha;
        private System.Windows.Forms.Button btnEntrar;
        private System.Windows.Forms.LinkLabel lblEsqueceuConta;
        private System.Windows.Forms.MaskedTextBox txtUsuario;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Label lblSenha;
        private System.Windows.Forms.CheckBox chkLembrarSenha;
        private System.Windows.Forms.Label lblUPWork;
    }
}