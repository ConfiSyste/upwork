﻿using ProjetoADMPersonalizadora.Login;
using ProjetoADMPersonalizadora.Login.EsqueceuConta;
using ProjetoADMPersonalizadora.Login.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoADMPersonalizadora.NewFolder1
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FrmEsqueceuConta ir = new FrmEsqueceuConta();
            ir.Show();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            if (txtUsuario.Text == string.Empty || txtSenha.Text == string.Empty)
            {
                MessageBox.Show("Campos vazios", "UP-Work", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtUsuario.Text = string.Empty;
                txtSenha.Text = string.Empty;
            }

            else
            {

                string usuario = txtUsuario.Text;
                string senha = txtSenha.Text;

                LoginBusiness bu = new LoginBusiness();
                LoginDTO login = bu.LOGAR(usuario, senha);

                if (login != null)
                {
                    SessaoUsuario.UsuarioLogado = login;
                    FrmMenu t = new FrmMenu();
                    t.Show();
                    Hide();
                }

                else
                {
                    MessageBox.Show("Credenciais inválidas!", "UP-Work", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Back || Char.IsLetter(e.KeyChar) == true || Char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = false;
                if (txtUsuario.Text.Length > 12 && e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Back || Char.IsLetter(e.KeyChar) == true || Char.IsNumber(e.KeyChar) == true || e.KeyChar == '@' || e.KeyChar == '-' || e.KeyChar == '.' || e.KeyChar == '_')
            {
                e.Handled = false;
                if (txtSenha.Text.Length > 16 && e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtUsuario_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}
