﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoADMPersonalizadora.NewFolder1
{
    class LoginDTO
    {
        public int ID_FUNCIONARIO { get; set; }
        public int ID_PERMISSAO { get; set; }
        public string NM_FUNCIONARIO { get; set; }
        public string USUARIO { get; set; }
        public string SENHA { get; set; }
        public string EMAIL { get; set; }
        public string CARGO { get; set; }
        public string CPF_FUNCIONARIO { get; set; }
        public DateTime DT_CADASTRO { get; set; }
       
    }
}
