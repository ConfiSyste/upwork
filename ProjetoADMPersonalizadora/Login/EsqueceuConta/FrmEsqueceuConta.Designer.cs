﻿namespace ProjetoADMPersonalizadora.Login.EsqueceuConta
{
    partial class FrmEsqueceuConta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.btnInscreverse = new System.Windows.Forms.Button();
            this.lblSobrenome = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(147, 50);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(139, 23);
            this.textBox6.TabIndex = 69;
            // 
            // btnInscreverse
            // 
            this.btnInscreverse.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.btnInscreverse.Location = new System.Drawing.Point(302, 50);
            this.btnInscreverse.Name = "btnInscreverse";
            this.btnInscreverse.Size = new System.Drawing.Size(81, 24);
            this.btnInscreverse.TabIndex = 68;
            this.btnInscreverse.Text = "Enviar";
            this.btnInscreverse.UseVisualStyleBackColor = true;
            // 
            // lblSobrenome
            // 
            this.lblSobrenome.AutoSize = true;
            this.lblSobrenome.BackColor = System.Drawing.Color.Transparent;
            this.lblSobrenome.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblSobrenome.Location = new System.Drawing.Point(23, 49);
            this.lblSobrenome.Name = "lblSobrenome";
            this.lblSobrenome.Size = new System.Drawing.Size(118, 21);
            this.lblSobrenome.TabIndex = 67;
            this.lblSobrenome.Text = "Email/Usuário:";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Century Gothic", 8F);
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(81, 24);
            this.button1.TabIndex = 70;
            this.button1.Text = "Voltar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmEsqueceuConta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 95);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.btnInscreverse);
            this.Controls.Add(this.lblSobrenome);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmEsqueceuConta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmEsqueceuConta";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Button btnInscreverse;
        private System.Windows.Forms.Label lblSobrenome;
        private System.Windows.Forms.Button button1;
    }
}