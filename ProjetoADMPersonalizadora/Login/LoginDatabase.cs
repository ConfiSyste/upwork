﻿using MySql.Data.MySqlClient;
using ProjetoADMPersonalizadora.Login.Menu.Orcamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoADMPersonalizadora.NewFolder1
{
    class LoginDatabase
    {
        public LoginDTO LOGAR(string usuario, string senha)
        {
            string script =
                @"SELECT * FROM TB_FUNCIONARIO
                    WHERE USUARIO_FUNCIONARIO = @USUARIO_FUNCIONARIO
                    AND   SENHA_FUNCIONARIO   = @SENHA_FUNCIONARIO";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("USUARIO_FUNCIONARIO", usuario));
            parms.Add(new MySqlParameter("SENHA_FUNCIONARIO", senha));

            Database.Database db = new Database.Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            LoginDTO login = null;

            if (reader.Read())
            {
                login = new LoginDTO();

                login.ID_FUNCIONARIO = reader.GetInt32("ID_FUNCIONARIO");
                login.ID_PERMISSAO = reader.GetInt32("ID_PERMISSAO");
                login.NM_FUNCIONARIO = reader.GetString("NM_FUNCIONARIO");
                login.USUARIO= reader.GetString("USUARIO_FUNCIONARIO");
                login.SENHA= reader.GetString("SENHA_FUNCIONARIO");
                login.EMAIL = reader.GetString("EMAIL_FUNCIONARIO");
                login.CARGO = reader.GetString("CARGO_FUNCIONARIO");
                login.CPF_FUNCIONARIO = reader.GetString("CPF_FUNCIONARIO");
                login.DT_CADASTRO = reader.GetDateTime("DT_CADASTRO_FUNCIONARIO");
               
            }

            reader.Close();
            return login;
        }
    }
}
